from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from jkh.models import UserProfile, Probl
from django.views.decorators.csrf import csrf_exempt

from viberbot.api.viber_requests import ViberConversationStartedRequest
from viberbot.api.viber_requests import ViberFailedRequest
from viberbot.api.viber_requests import ViberMessageRequest
from viberbot.api.viber_requests import ViberSubscribedRequest
from viberbot.api.viber_requests import ViberUnsubscribedRequest
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration
from viberbot.api.messages import VideoMessage
from viberbot.api.messages.text_message import TextMessage
from viberbot.api.messages.keyboard_message import KeyboardMessage
from jkh.classifikation import classification









def index(request):
    problems = Probl.objects.all()
    return render(request, 'requests.html', {'problems': problems})

def users(request):
    return render(request, 'users.html', { 'profiles': UserProfile.objects.all()})

@csrf_exempt
def message(request):
    viber = Api(BotConfiguration(
        name='JkhHelper',
        avatar='https://jkh-helper.tk/static/images/viber.png',
        auth_token='49f07a194727d717-c0792ba113058b69-7c6d00e64ef909f6'
    ))
    #print("received request. post data: {0}".format(request.get_data()))
    # if not viber.verify_signature(request, request.headers.get('X-Viber-Content-Signature')):
    #     return HttpResponseForbidden()

    viber_request = viber.parse_request(request.body.decode())

    if isinstance(viber_request, ViberMessageRequest):
        user_id = viber_request.sender.id
        user_avatar=viber_request.sender.avatar
        message=viber_request.message


        if len(UserProfile.objects.filter(ViberId=user_id)) == 0:
            user = UserProfile.objects.filter(Phone=message.text)

            if len(user)==1:
                user[0].ViberId = user_id
                user[0].IsActive = True
                user[0].save()
                viber.send_messages(user_id, [
                    TextMessage(text='Номер телефона подтвержден')
                ])
            else:
                viber.send_messages(user_id, [
                    TextMessage(text='Номер не найден в базе')
                ])
            return HttpResponse(status=200)
        else:
            text = viber_request.message.text
            media = viber_request.message.media
            if text == None or media == None:
                viber.send_messages(user_id, [
                    TextMessage(text='Пришлите фотографию и описание своей проблемы и мы обязательно решим ее')
                ])
            else:
                problem = Probl(Profile=UserProfile.objects.filter(ViberId=user_id)[0], Text=text, Image=media, Specialist=classification(text))
                problem.save()
                viber.send_messages(user_id, [
                    TextMessage(text='Ваш запрос зарегистрирован, ожидайте звонка')
                ])









    return HttpResponse('')

