from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    User = models.OneToOneField(User, on_delete=models.CASCADE)
    FirstName = models.CharField('Имя', max_length=100, blank=False)
    LastName = models.CharField('Фамилия', max_length=100, blank=False)
    FatherName = models.CharField('Отчество', max_length=100, blank=False)
    Phone = models.CharField('телефон', max_length=100, blank=True)
    Adress = models.CharField('Адрес', max_length=100, blank=False)
    ViberId = models.CharField('viber id', max_length=100, blank=True)
    IsActive = models.BooleanField('Подтверждение')


class Probl(models.Model):

    Profile = models.ForeignKey(UserProfile, verbose_name='Пользователь', on_delete=models.PROTECT)
    Text = models.CharField('Описание', max_length=550, blank=True)
    Image = models.CharField('Картинка', max_length=550, blank=True)
    Specialist = models.CharField('Специалист', max_length=100, blank=True)

