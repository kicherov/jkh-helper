


def classification(_text):
    keywords = {'Электрик':  ['провод', 'счетчик', 'провода', 'ток'],
                'Сантехник': ['вода', 'труба', 'топят', 'прорвало']}
    for word in keywords['Электрик']:
        if word in _text:
            return 'Электрик'
    for word in keywords['Сантехник']:
        if word in _text:
            return 'Сантехник'
    return 'Разнорабочий'


