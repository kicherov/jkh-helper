# Generated by Django 2.2.3 on 2019-07-05 16:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jkh', '0004_auto_20190705_1439'),
    ]

    operations = [
        migrations.RenameField(
            model_name='probl',
            old_name='User',
            new_name='Profile',
        ),
    ]
